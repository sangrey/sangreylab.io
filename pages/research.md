.. title: Research
.. slug: research
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. hidetitle: True
.. has_math: yes


<div class="row", width=100%>
<div class="col-md-12">
<div class="media-heading">
<h1 align="center"> Research</h1>
<div>
</div>


<div class="row">
<div class="col-md-12">
<div class="media-body">

<h2 align=center> Identifying Volatility Risk Price Through Leverage Effect (<a
href=/risk_price_inference.pdf>pdf</a>) </h2> 

<h3 align=center> 
with <a href="https://sites.google.com/site/xucheng06/">Xu Cheng</a> (University of Pennsylvania) and <a
href="https://warwick.ac.uk/fac/soc/economics/staff/emrrenault/">Eric Renault</a> (University of Warwick)
</h3>


<h3 align=center> 
forthcoming in the Journal of Econometrics 
</h3>

<p, align="justify"> 
In asset pricing models with stochastic volatility, uncertainty about volatility affects risk premia through two channels: aversion to decreasing returns and aversion to increasing volatility. We analyze the identification of and robust inference for structural parameters measuring investors' aversions to these risks: the return risk price and the volatility risk price. In the presence of a leverage effect (instantaneous causality between the asset return and its volatility), we study the identification of both structural parameters with the price data only, without relying on additional option pricing models or option data. We analyze this identification challenge in a nonparametric discrete-time exponentially affine model, complementing the continuous-time approach of Bandi and Renò (2016). We then specialize to a parametric model and derive the implied minimum distance criterion relating the risk prices to the asset return and volatility's joint distribution. This criterion is almost flat when the leverage effect is small, and we introduce identification-robust confidence sets for both risk prices regardless of the magnitude of the leverage effect.
</p>
</div>
</div>
</div>

<div class="row">
<div class="col-md-12">
<div class="media-body">

<h2 align=center> Cross-sectional State-Space Forecasting with Partial Pooling(<a
href=/cross_sectional_forecasting_with_partial_pooling.pdf>pdf</a>) </h2> 

<h3 align=center> with <a href="https://www.linkedin.com/in/michael-bedard-717899175/">Mike Bedard</a> (Amazon)
and <a href="https://www.linkedin.com/in/mattjohnsonphd/">Matt Johnson</a> (Amazon)
</h3>

<p, align="justify"> 
We propose a novel architecture for time series models built upon state-space methods. We jointly estimate many, potentially multivariate, distributions defined using state-space models by partially pooling their parameters across the cross- section. These joint distributions define a novel recurrent neural network. By combining state-space methods and neural networks, we leverage the interpretability of state-space models and the scalability and flexibility of neural networks. This lets us build an accurate, flexible, and scalable forecast that is not a black box. We implement this architecture by building a library on the deep learning library MXNet to leverage state-of-the-art scalable optimization techniques including automatic differentiation and computation graphs to estimate the parameters governing the state-space models. This library abstracts over a large class of state-space models allowing users to estimate almost arbitrary model specifications without code changes. We forecast weekly business formation by state, which is obtained from the FRED economic database at the St. Louis Federal Reserve bank. We show that this forecast is more accurate than state-of-the-art neural network approaches (DeepState) and more accurate than state-of-the-art univariate generalized linear models (Prophet) when the data are particularly volatile.
</p>
</div>
</div>
</div>

<div class="row">
<div class="col-md-12">
<div class="media-body">

<h2 align=center> Feasible Multivariate Density Estimation using Random Compression (<a
href=/feasible_multivariate_density_estimation.pdf>pdf</a>) </h2> 

<h3 align=center> with <a href="http://minsuchang.com">Minsu Chang</a> (Georgetown University)
</h3>

<p, align="justify"> 
Given vector-valued data span <span style:"white-space:nowrap"> {x<sub>t</sub>} </span>, nonparametric density estimators typically converge slowly when the number of series D is large. We extend ideas from the random compression literature to nonparametric density estimation, constructing an estimator that, with high probability, converges rapidly even when applied to a large, fixed number of series. We devise a discrete random operator to compress the data so that the density of the compressed data can be represented as a parsimonious mixture of Gaussians. We show that this mixture representation closely approximates the true distribution. Then we provide a computationally efficient Gibbs sampler to construct our Bayesian density estimator using Dirichlet mixture models. We estimate both marginal and transition densities for both i.i.d. and Markov data. With high probability with respect to the randomness of the compression, our estimators’ convergence rate &mdash; <span style:"white-space:nowrap">log(T) / &radic;<span style="text-decoration:overline;">T</span></span> &mdash; depends on D only through the constant term. Our procedure produces a well-calibrated joint predictive density for a macroeconomic panel.
</p>
</div>
</div>
</div>

<div class="row">
<div class="col-md-12">
<div class="media-body">

<h2 align=center>Jumps, Realized Densities, and News Premia (<a href=/realized_densities.pdf>pdf</a>)</h2>
<p, align="justify">
Announcements and other news continuously barrage financial markets, causing asset prices to jump hundreds of times each day. If price paths are continuous, the diffusion volatility nonparametrically summarizes the return distributions' dynamics, and risk premia are instantaneous covariances. However, this is not true in the empirically-relevant case involving price jumps. To address this impasse, I derive both a tractable nonparametric continuous-time representation for the price jumps and an implied sufficient statistic for their dynamics. This statistic &mdash; jump volatility &mdash; is the instantaneous variance of the jump part and measures news risk. The realized density then depends, exclusively, on the diffusion volatility and the jump volatility. I develop estimators for both and show how to use them to nonparametrically identify continuous-time jump dynamics and associated risk premia. I provide a detailed empirical application to the S&P 500 and show that the jump volatility premium is less than the diffusion volatility premium.  
</p>
</div>
</div>
</div>

</div>


