.. title: Links
.. slug: links
.. date: 2017-04-07 09:55:15 UTC-04:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. hidetitle: True

<div class="row", width=100%>
<div class="col-md-12">
<div class="media-heading">
 <h2, align="left"> Personal Pages:</h2>
<div>
</div>


<div class="row">
<div class="col-md-12">
<div class="media-body">

<a href="https://www.linkedin.com/in/paul-sangrey-b5070871/"><h3>LinkedIn</h3></a>
<a href="https://gitlab.com/sangrey"><h3>Gitlab</h3></a> 

</div>
</div>
</div>

<div class="row">
<div class="col-md-12">
<div class="media-body">
 <h2, align="left"> Python / C++ Projects:</h2>
</div>
</div>
</div>

<div class="row">
<div class="col-md-12">
<div class="media-heading">

<a href="https://anaconda.org/sangrey/arma_wrapper"><h3>arma_wrapper</h3></a>
  <p> This package enables converting NumPy arrays to C++ Armadillo matrices. It is essentially RcppArmadillo for 
   Python. It is built on top of <a href="http://pybind11.readthedocs.io">Pybind11</a>. <a
   href="https://gitlab.com/sangrey/Arma_Wrapper.git">(Gitlab)</a>

 <a href="https://anaconda.org/sangrey/cdpm"><h3>cdpm</h3></a>
   <p> This package provides code for Bayesian nonparametric conditional density estimation. It is  associated
   with <i> Feasible Multivariate Density Estimation.</i> <a
   href="https://gitlab.com/sangrey/cdpm2.git">(Gitlab)</a>

 <a href="https://anaconda.org/sangrey/laplacejumps"><h3>laplacejumps</h3></a>
  <p> This package provides code for realized volatility estimation in the presence of jumps. It is associated
  with <i>Jumps, Realized Densities, and News Premia.</i> <a
  href="https://gitlab.com/sangrey/LaplaceJumps.git">(Gitlab)</a>

</div>
</div>
</div>

<div class="row">
<div class="col-md-12">
<div class="media-heading">
 <h2, align="left"> Coauthors:</h2>
</div>
</div>
</div>

<div class="row">
<div class="col-md-12">
<div class="media-heading">

   <a href="https://www.linkedin.com/in/michael-bedard-717899175/"><h3>Mike Bedard</h3></a>
   <a href="http://minsuchang.com/"><h3>Minsu Chang</a></h3></li>
   <a href="https://sites.google.com/site/xucheng06/"><h3>Xu Cheng</a><h3>
   <a href="https://www.linkedin.com/in/mattjohnsonphd/"><h3>Mike Johnson</h3></a>
   <a href="https://warwick.ac.uk/fac/soc/economics/staff/emrrenault/"><h3>Eric Renault</h3></a>
   

</div>
</div>
</div>


</div>

