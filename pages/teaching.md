.. title: Teaching
.. slug: teaching
.. date: 2017-04-07 09:55:15 UTC-04:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. hidetitle: True


<div class="row", width=100%>
<div class="col-md-12">
<div class="media-heading">
<h1 align="center"> Teaching Experience</h1>
<div>
</div>

<div class="row">
<div class="col-md-6">

<div class="media-heading">
 <h2 align="left"> Classes:</h2>
</div>


<div class="media-body">
 <h3 align="left"> Econ 050 International Economics (<a
 href=/international_economics_syllabus.pdf>Syllabus</a>):</h3> 

<p align="justify"> 
I taught a sophomore-level class on international economics in the Summer of 2016 and the
Spring of 2017. 
Before the midterm, I focused on international trade, and afterwards I covered international finance.
Topics I covered included Ricardo's model of comparative advantage, economies of scale, how purchasing power
parity affects exchange rates, and covered interest parity.  
Throughout the course, I strove to make the class relevant to both the students who would continue in economics
and those specialize in other fields. 
I did this by encouraging discussion in class, bringing in current events, and helping the students develop their
presentation, writing, and quantitative skills through relevant assignments.
</p>

</div>


</div>
<div class="col-md-6">

<div class="media-heading">
 <h2 align="left"> Tutorials:</h2>
</div>

<div class="media-body">

<a href="/git_tutorial.pdf"><h3>From Commits To Collaboration: A Git Tutorial for Social Scientists</h3></a>      

<a href=/time_series_intro.pdf><h3> Introduction to Time Series </h3></a>

</div>
</div>
</div>
</div>
