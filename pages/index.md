.. title: Paul Sangrey
.. slug: index
.. date: 2022-11-12 12:00:00 UTC-04:00
.. tags: econometrics, economics, finance, University of Pennsylvania, big data, Bayesian, research, machine
learning, asset pricing, learning, uncertainty, high-frequency, explainable AI, AI, state-space, Noom, state-space
.. category: 
.. link: 
.. description: Welcome! I am an economist at Noom working on forecasting for business finance.
.. type: text
.. hidetitle: True


                 
<div class="row media-body no-gutters" width=100%>  

<div class="mx-auto col-md-12 col-lg-3 text-center">

<img class="mx-auto d-block img-fluid" src="/images/september_2018_headshot-min.jpg" alt="Paul Sangrey" style="max-height:320px"> 
<div class="w-100"></div>

<address class="d-block">                                                                                             
<br>
PhD Economist <br>                                                                                  
Noom <br>                                                                                    
<a href="mailto:paul@sangrey.io">paul@sangrey.io</a> <br>
</address>                                                                                                         
</div>


<div class="col-md-12 col-lg-9">

<div class="media-heading">
<h1, align="center"> Paul Sangrey </h1>                                                                            
</div>                                                                                                             


<div class="media-body"> 
<p align="justify"> 
Welcome! I am a senior economist at Noom, where I came after working at Amazon's premier business data science team for a few years. I develop methods to relate business performance and customer behavior to long-term financial success.  We live in a world where vast new datasets are becoming available every day. This lets us rigorously identify metrics that summarize the key information in the data for decision makers in real time. Against that background, I develop explainable forecasts methods at the intersection of traditional time series econometrics and machine learning to produce actionable insights in real time. 
</p>

In <a href=/cross_sectional_forecasting_with_partial_pooling.pdf>Cross Sectional Forecasting with Partial Pooling</a>, my coauthors and I leverage the explainability of state-space models and the scalability and flexibility of neural networks to build an accurate, interpretable forecast that is not a black box and outperforms both neural networks and univariate methods since Covid-19. In <a href=/feasible_multivariate_density_estimation.pdf><em>Feasible Multivariate Density Estimation using Random Compression</em></a>, we develop Bayesian methods to feasibly estimate the joint density of 5 to 10 variables without distributional assumptions and develop new theory to explain why this is feasible. In <a href=/realized_densities.pdf><em>Jumps, Realized Densities, and News Premia</em></a>, I build an interpretable nonparametric framework relating high-frequency and daily returns. This framework provides a novel sufficient statistic for time-varying news risk &mdash; <em>jump volatility</em> &mdash; which I estimate using high-frequency data. In <a href=/risk_price_inference.pdf><em>Identifying Volatility Risk Price Through Leverage Effect</em></a>, we provide identification-robust inference for the prices of market and volatility risk in the presence of stochastic volatility and leverage effects. 
</p>

</div>
</div>
</div>
